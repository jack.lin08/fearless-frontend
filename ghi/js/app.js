function createCard(name, description, location, pictureUrl, startDate, endDate) {
    return `
    <div class='card shadow p-3 mb-5 bg-body-tertiary rounded'>
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
              <h5 class="card-title">${name}</h5>
              <h6 class="text-secondary">${location}</h6>
              <p class="card-text">${description}</p>
            </div>
            <div class="footer">${startDate} - ${endDate}
            </div>
          </div>
          `;
}

function createError() {
    return `
        <div>
        <h1 class="alart alert-danger role=alert">
        Bad request!
        </h1>
        </div>
    `
}


window.addEventListener('DOMContentLoaded', async() => {

    const url = 'http://localhost:8000/api/conferences/';

    try{


    const response = await fetch(url);

    if(!response.ok){
        const html2 = createError()
        const main = document.querySelector('.row')
        main.innerHTML += html2
    } else{
        const data = await response.json();
        let counter = 0

        for (let conference of data.conferences){


        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {

            const colNum = (counter%3);
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const sDate = new Date(details.conference.starts);
            const eDate = new Date(details.conference.ends);
            const options = {month: 'numeric', year: 'numeric', day: 'numeric'}
            const startDate = sDate.toLocaleDateString(undefined, options);
            const endDate = eDate.toLocaleDateString(undefined, options);
            const location = details.conference.location.name

            const html = createCard(title, description, location, pictureUrl, startDate, endDate);
            let column = document.querySelector(`.col${colNum}`);
            column.innerHTML += html;
            counter += 1;




            //const detailNameTag = document.querySelector('.card-text');
            //detailNameTag.innerHTML = description;

            //const imageTag = document.querySelector('.card-img-top');
            //imageTag.src = details.conference.location.picture_url
            //console.log(details);
        }
        }
    }
} catch (e) {
    console.error(error);
}



});
